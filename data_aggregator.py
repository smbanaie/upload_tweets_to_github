import requests
import zipfile
import io
import json
from config import *
from datetime import datetime
from github import Github
from github import InputGitTreeElement
import base64


def get_column_names():
    columns = "{:>20}\t{:>15}\t{:>20}\t{:>50}\t".format("tw_id", "tw_created_at", "user_id", "user_screen_name")
    columns += "{:>50}\t{:>100}\t{:>100}\t{:>8}\t".format("name", "user_description", "user_location",
                                                          "user_followers_count")
    columns += "{:>8}\t{:>8}\t{:>8}\t{:>8}\t".format("user_friends_count", "user_listed_count", "user_statuses_count",
                                                     "user_favourites_count")
    columns += "{:>20}\t{:>20}\t{:>20}\t{:>10}\t".format("user_created_at", "tw_favorite_count", "tw_retweet_count",
                                                         "tw_is_quote")
    columns += "{:>15}\t{:>50}\t{:>12}\t{:>10}\t".format("tw_media_type", "tw_hashtags", "tw_has_url", "tw_is_quote")
    columns += "{:>20}\t{:>280}".format("is_original_tweet", "tw_text")
    # retweet info comes after the tweet data
    return columns + "\t" + columns + "\r\n"


def insert_column_names():
    out_file = open(CONFIG.EXTRACT_FILE, "w", encoding="utf-8")
    out_file.write(get_column_names())
    out_file.close()


def check_config_values(config):
    if not config.PROJECT_ID:
        raise ValueError("PROJECT_ID not set in config")
    elif not config.GITLAB_URL:
        raise ValueError("GITLAB_URL not set in config")
    elif not config.API_KEY:
        raise ValueError("API_KEY not set in config")
    elif not config.EXTRACT_FILE:
        raise ValueError("EXTRACT_FILE not set in config")


def get_finished_jobs():
    all_jobs = []
    job_count = 0
    page = 1
    while True:
        url = (CONFIG.GITLAB_URL + "projects/{0}/jobs?scope[]=success&page={1}").format(CONFIG.PROJECT_ID, page)
        r = requests.get(url, headers={"PRIVATE-TOKEN": CONFIG.API_KEY})
        jobs_in_page = json.loads(r.content)
        if len(jobs_in_page) == 0:
            break
        jobs_in_6h = []
        finished = False
        for job in jobs_in_page:
            job_count += 1
            print("{0} : Job ID : {1} - Page {2}".format(job_count, job["id"], page))
            finished_at = datetime.strptime(job["finished_at"], "%Y-%m-%dT%H:%M:%S.%fz")
            hours_past = (datetime.now() - finished_at).days * 24 + (datetime.now() - finished_at).seconds // 3600
            print("\t Hours Past : {0} - Finished At : {1}".format(hours_past, job["finished_at"]))
            if hours_past < 6:
                jobs_in_6h.append(job)
            else:
                finished = True
                break
        if finished:
            all_jobs.extend(jobs_in_6h)
            break

        page += 1
    return all_jobs


def download_artifact(job_id):
    try:
        url = (CONFIG.GITLAB_URL + "projects/{0}/jobs/{1}/artifacts").format(CONFIG.PROJECT_ID, job_id)
        print("\t\tDownloading {}".format(str(job_id)))
        r = requests.get(url, headers={"PRIVATE-TOKEN": CONFIG.API_KEY}, stream=True)
        print("\t\tDownloading {} Finished".format(str(job_id)))
        z = zipfile.ZipFile(io.BytesIO(r.content))
        # if not os.path.exists(extract_dir+"\\"+str(job_id)):
        #     os.makedirs(extract_dir+"\\"+str(job_id))
        z.extractall()
        in_file = open(CONFIG.INPUT_FILE, "r", encoding="utf-8")
        out_file = open(CONFIG.EXTRACT_FILE, "a", encoding="utf-8")
        header_line = next(in_file)
        for line in in_file:
            out_file.write(line)
        out_file.close()
        print("\t\tJob Artifacts Saved")
    except Exception as e:
        print(e)


def download_all_artifacts():
    try:
        print("Job Fetch Started : ")
        jobs = get_finished_jobs()
        print("\tDownload Phase Started ")
        cnt = 0
        for j in jobs:
            cnt += 1
            print("\t{} of {} : Downloading Job  {} issued".format(str(cnt), str(len(jobs)), j["id"]))
            download_artifact(j['id'])
    except Exception as e:
        print(e)


def compress_tweet_datafiles():
    zipfile_ame = 'data.zip'
    compression = zipfile.ZIP_DEFLATED
    zf = zipfile.ZipFile(zipfile_ame, mode='w')
    zf.write(CONFIG.EXTRACT_FILE, compress_type=compression)
    zf.close()
    return zipfile_ame


def upload_file_to_github(file_name):
    print("Github Uploading started ...")
    user = "smbanaie"
    password = "odiF2t11#"
    g = Github(user, password)
    repo = g.get_user().get_repo('TweetData')
    now = datetime.now()
    today = now.strftime("%Y-%m-%d")
    commit_message = 'Adding Daily Tweets - {}'.format(today)
    master_ref = repo.get_git_ref('heads/master')
    master_sha = master_ref.object.sha
    base_tree = repo.get_git_tree(master_sha)
    element_list = list()
    data = base64.b64encode(open(file_name, "rb").read())
    print("Zip File Len is  : {}".format(len(data)))
    path = "{}/{}/d{}_h{}_{}".format(now.year, now.month, now.day, now.hour, "tweets.zip")
    blob = repo.create_git_blob(data.decode("utf-8"), "base64")
    element = InputGitTreeElement(path=path, mode='100644', type='blob', sha=blob.sha)
    element_list.append(element)

    tree = repo.create_git_tree(element_list, base_tree)
    parent = repo.get_git_commit(master_sha)
    commit = repo.create_git_commit(commit_message, tree, [parent])
    master_ref.edit(commit.sha)
    print("Upload Finished")


insert_column_names()
check_config_values(CONFIG)
download_all_artifacts()
output_file = compress_tweet_datafiles()
upload_file_to_github(output_file)

